package pl.sda.chlodnictwo;

public class Main {
    public static void main(String[] args) {
        Grzeje urzadzenie = new Farelka(21);
         urzadzenie.zwiekszTemp();
        urzadzenie.wyswietlTemperature();

        Chlodzi klima = new Klimatyzacja(30);
        ((Klimatyzacja) klima).zwiekszTemp();
        klima.wyswietlTemperature();


        Chlodzi wiatraczek = new Wiatrak(12);
        wiatraczek.schlod();
        wiatraczek.wyswietlTemperature();

    }
}
