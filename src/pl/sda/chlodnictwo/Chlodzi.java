package pl.sda.chlodnictwo;

public interface Chlodzi {
    double pobierzTemp();
    void schlod();
    default void wyswietlTemperature(){
        System.out.println("Aktualna temperatura w pomieszczeniu wynosi "+pobierzTemp()+" stopni Celsjusza");
    }
}
