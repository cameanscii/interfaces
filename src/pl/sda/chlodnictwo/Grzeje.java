package pl.sda.chlodnictwo;

public interface Grzeje {
    double pobierzTemp();
    void zwiekszTemp();
    default void wyswietlTemperature(){
        System.out.println("Aktualna temperatura w pomieszczeniu wynosi "+pobierzTemp()+" stopni Celsjusza");
    }
}
