package pl.sda.chlodnictwo;

public class Klimatyzacja implements Grzeje,Chlodzi{
    private double temperatura ;

    public Klimatyzacja(double temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public void schlod() {
        temperatura-=1.2;
    }

    @Override
    public double pobierzTemp() {
        return temperatura;
    }

    @Override
    public void zwiekszTemp() {
        temperatura+=1.2;
    }

    @Override
    public void wyswietlTemperature() {
        System.out.println("Aktualna temperatura w pomieszczeniu wynosi "+pobierzTemp()+" stopni Celsjusza");
    }
}
