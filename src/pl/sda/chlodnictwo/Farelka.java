package pl.sda.chlodnictwo;

public class Farelka implements Grzeje {

    private double temperatura ;

    public Farelka(double temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public double pobierzTemp() {
        return temperatura;
    }

    @Override
    public void zwiekszTemp() {
        temperatura+=1.2;
    }
}
