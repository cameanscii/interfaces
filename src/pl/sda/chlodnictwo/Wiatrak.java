package pl.sda.chlodnictwo;

public class Wiatrak implements Chlodzi {
    private double temperatura ;

    public Wiatrak(double temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public double pobierzTemp() {
        return temperatura;
    }

    @Override
    public void schlod() {
        temperatura-=1.2;
    }
}
