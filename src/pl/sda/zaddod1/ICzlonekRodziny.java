package pl.sda.zaddod1;

public interface ICzlonekRodziny {
    void przedstawSie();
    boolean jestDorosly();
}
