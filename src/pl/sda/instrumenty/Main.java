package pl.sda.instrumenty;

public class Main {
    public static void main(String[] args) {


        Instrumentalny beben = new Beben();
        Instrumentalny gitara = new Gitara();
        Instrumentalny pianino = new Pianino();

        beben.graj();
        gitara.graj();
        pianino.graj();

    }
}
