package pl.sda.ludki;

public class Student implements Comparable{
    String imie;
    String nazwisko;
    int nrAlbumu;

    public Student(String imie, String nazwisko, int nrAlbumu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrAlbumu = nrAlbumu;
    }

    @Override
    public int compareTo(Object o) {
//        Student janek = (Student) o;
//Integer.compare(nrAlbumu,janek.nrAlbumu);
        return this.nrAlbumu-((Student)o).nrAlbumu;
    }


    @Override
    public String toString() {
        return this.imie + " "+ this.nazwisko+" "+this.nrAlbumu;
    }
}
