package pl.sda.ludki;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student jankoMuzykant=new Student("Janko", "Muzykant",123);
        Student marek=new Student("Marek", "Muzykanto",121);
        Student jolaLojalna=new Student("Jolka", "CzyPamietasz",123000);
        Student fragles=new Student("Muppet", "Pokemon",13);

        Student[] studenci = new Student[]{jankoMuzykant,marek,jolaLojalna,fragles};
        System.out.println(Arrays.toString(studenci));
        Arrays.sort(studenci);
        System.out.println(Arrays.toString(studenci));

        System.out.println(fragles.compareTo(jankoMuzykant));

    }
}
