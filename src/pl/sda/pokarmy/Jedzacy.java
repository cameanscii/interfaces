package pl.sda.pokarmy;

public interface Jedzacy {
    void jedzPokarm(Pokarm pokarm);
    int ilePosilkowZjedzone();
    int ileGramowZjedzone();
    int ileKaloriiZjedzone();

}
