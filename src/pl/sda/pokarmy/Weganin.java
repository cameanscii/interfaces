package pl.sda.pokarmy;

public class Weganin  implements Jedzacy{

    private int iloscPosilkow;
    private int ileGram;
    private int ileKalorii;
    @Override
    public void jedzPokarm(Pokarm pokarm) {
        if(pokarm.typPokarmu==TypPokarmu.MIĘSO||pokarm.typPokarmu==TypPokarmu.NABIAŁ)
        {
            System.out.println("Nie zjem tego!");
        }else {
            System.out.println("Omomom pyszne: "+pokarm.nazwa);
            iloscPosilkow++;
            ileGram+=pokarm.waga;
            ileKalorii+=pokarm.waga*pokarm.typPokarmu.kcalNaGram;
        }
    }

    @Override
    public int ilePosilkowZjedzone() {
        return iloscPosilkow;
    }

    @Override
    public int ileGramowZjedzone() {
        return ileGram;
    }

    @Override
    public int ileKaloriiZjedzone() {
        return ileKalorii;
    }
}
