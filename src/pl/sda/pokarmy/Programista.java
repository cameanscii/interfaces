package pl.sda.pokarmy;

public class Programista implements Jedzacy {
    private int iloscPosilkow;
    private int ileGram;
    private int ileKalorii;
    @Override
    public void jedzPokarm(Pokarm pokarm) {
        if(pokarm.typPokarmu==TypPokarmu.OWOCE)
        {
            System.out.println("Nie zjem tego! Nie wiem co to?!");
        }else {
            System.out.println("Bardzo dobre bardzo pyszne: "+pokarm.nazwa);
            iloscPosilkow++;
            ileGram+=pokarm.waga;
            ileKalorii+=pokarm.waga*pokarm.typPokarmu.kcalNaGram;
        }
    }

    @Override
    public int ilePosilkowZjedzone() {
        return iloscPosilkow;
    }

    @Override
    public int ileGramowZjedzone() {
        return ileGram;
    }
    @Override
    public int ileKaloriiZjedzone() {
        return ileKalorii;
    }
}
