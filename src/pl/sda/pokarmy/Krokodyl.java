package pl.sda.pokarmy;

public class Krokodyl implements Jedzacy {
    private int iloscPosilkow;
    private int ileGram;
    private int ileKalorii;
    @Override
    public void jedzPokarm(Pokarm pokarm) {
        if(pokarm.typPokarmu==TypPokarmu.OWOCE||pokarm.typPokarmu==TypPokarmu.NABIAŁ)
        {
            System.out.println("ROAR!");
        }else {
            System.out.println("ROAR GRRR dobre: "+pokarm.nazwa);
            iloscPosilkow++;
            ileGram+=pokarm.waga;
           ileKalorii+=pokarm.waga*pokarm.typPokarmu.kcalNaGram;
        }
    }

    @Override
    public int ilePosilkowZjedzone() {
        return iloscPosilkow;
    }

    @Override
    public int ileGramowZjedzone() {
        return ileGram;
    }
    @Override
    public int ileKaloriiZjedzone() {
        return ileKalorii;
    }
}
